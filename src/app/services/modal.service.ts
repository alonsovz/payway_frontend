import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LibroEntity } from '../models/libro.entity';
import { AutorEntity } from '../models/autor.entity';
import { CategoriaEntity } from '../models/categoria.entity';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor() { }

  private modalStateCrear = new BehaviorSubject<boolean>(false);
  modalCrearState$ = this.modalStateCrear.asObservable();

  private modalStateEditar = new BehaviorSubject<boolean>(false);
  modalEditarState$ = this.modalStateEditar.asObservable();

  private modalStateEliminar = new BehaviorSubject<boolean>(false);
  modalEliminarState$ = this.modalStateEliminar.asObservable();

  openModalCrear() {
    this.modalStateCrear.next(true);
  }

  closeModalCrear() {
    this.modalStateCrear.next(false);
  }

  openModalEditar() {
    this.modalStateEditar.next(true);
  }

  closeModalEditar() {
    this.modalStateEditar.next(false);
  }


  openModalEliminar() {
    this.modalStateEliminar.next(true);
  }

  closeModalEliminar() {
    this.modalStateEliminar.next(false);
  }


 
}
