import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })

};

@Injectable({
  providedIn: 'root'
})
export class AutoresService {

  constructor(private http: HttpClient, private router: Router) { }


  public getAutores(): Observable<any> {
    return this.http.get(environment.apiBackend+ 'autores', httpOptions).pipe(map(data => data as any));
  }
}
