import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LibroEntity } from '../models/libro.entity';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })

};
@Injectable({
  providedIn: 'root'
})
export class LibrosService {

  
  private refresh: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public getRefresh(): Observable<boolean>{
    return this.refresh.asObservable();
  }

  public setRefresh(value: boolean): void{
    this.refresh.next(value);
  }

  constructor(private http: HttpClient, private router: Router) { }


  public getLibros(): Observable<any> {
    return this.http.get(environment.apiBackend+ 'libros', httpOptions).pipe(map(data => data as any));
  }

  public save(datos: LibroEntity) : Observable<LibroEntity>{
    return this.http.post<LibroEntity>(environment.apiBackend+'libros', datos, httpOptions).pipe(map(data=>data as LibroEntity))
  }

  public edit(datos: LibroEntity) : Observable<LibroEntity>{
    return this.http.put<LibroEntity>(environment.apiBackend+'obtener-libros/' + datos.id, datos, httpOptions).pipe(map(data=>data as LibroEntity))
  }


  public delete(datos: LibroEntity) : Observable<LibroEntity>{
    return this.http.put<LibroEntity>(environment.apiBackend+'obtener-libros/' + datos.id, datos, httpOptions).pipe(map(data=>data as LibroEntity))
  }
}
