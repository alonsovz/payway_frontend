import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AutorEntity } from 'src/app/models/autor.entity';
import { CategoriaEntity } from 'src/app/models/categoria.entity';
import { LibroEntity } from 'src/app/models/libro.entity';
import { LibrosService } from 'src/app/services/libros.service';
import { ModalService } from 'src/app/services/modal.service';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  form!: FormGroup;

  @Input()  categorias : any;
  @Input()  autores : any;
  @Input()  libro : LibroEntity = new LibroEntity();

  constructor(public modalService: ModalService, private libros_service: LibrosService) {
    this.form = new FormGroup({
      'titulo' : new FormControl('', [Validators.required]),
      'precio' : new FormControl('', [Validators.required]),
      'autor_id' : new FormControl('', [Validators.required]),
      'categoria_id' : new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {}

  closeModal() {
    this.modalService.closeModalEditar();
  }
  save(){
    let categoria : CategoriaEntity = new CategoriaEntity();
    categoria.id = this.form.controls["categoria_id"].value;

    let autor: AutorEntity = new AutorEntity();
    autor.id = this.form.controls["autor_id"].value;

    let libro : LibroEntity = new LibroEntity();
    libro.autor = autor;
    libro.categoria = categoria;
    libro.titulo = this.form.controls["titulo"].value;
    libro.precio = this.form.controls["precio"].value;
    libro.estado = true;
    libro.id = this.libro.id;

    this.libros_service.edit(libro).subscribe(
      {
        next: (response)=>{
          Swal.fire({
            icon: "success",
            title: "Confirmación!",
            text: "Libro modificado con éxito!",
          });
          this.libros_service.setRefresh(true);
          this.modalService.closeModalEditar();
        },error:(err)=>{
          Swal.fire({
            icon: "error",
            title: "Oops!",
            text: "Algo salió mal!",
          });
        }
      }
    );
  }

}
