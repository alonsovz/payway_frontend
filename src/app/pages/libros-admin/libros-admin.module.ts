import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibrosAdminRoutingModule } from './libros-admin-routing.module';
import { ViewComponent } from './view/view.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ViewComponent,
    CreateComponent,
    EditComponent,
    DeleteComponent
  ],
  imports: [
    CommonModule,
    LibrosAdminRoutingModule,
    ReactiveFormsModule
  ]
})
export class LibrosAdminModule { }
