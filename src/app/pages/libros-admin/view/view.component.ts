import { Component, OnInit } from '@angular/core';
import { LibroEntity } from 'src/app/models/libro.entity';
import { AutoresService } from 'src/app/services/autores.service';
import { CategoriasService } from 'src/app/services/categorias.service';
import { LibrosService } from 'src/app/services/libros.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  items: any[] = []; // Your list of items
  currentPage = 1;
  itemsPerPage = 10;
  libros_data: any;
  categorias_data: any;
  autores_data: any;
  mostrarModal: boolean = false;
  libro: LibroEntity = new LibroEntity();
  constructor(private libro_service: LibrosService, private modalService: ModalService, private categoria_service: CategoriasService, private autores_service: AutoresService){}


  ngAfterViewInit() {
    this.libro_service.setRefresh(true);
    this.obtener_list();
    this.getAutores();
    this.getCategorias();
  }

  obtener_list(){
    this.libro_service.getRefresh().subscribe((value: boolean) =>{
      if(value){
        this.getlibros();
      }
    })
  }

  getlibros(){
    this.libro_service.getLibros().subscribe(
      {
        next:(response) => {
          this.libros_data = response;
        },
        error:(err) =>{
        }
      }
    );
  }

  getCategorias(){
    this.categoria_service.getCategorias().subscribe(
      {
        next:(response) => {
          this.categorias_data = response;
        },
        error:(err) =>{
        }
      }
    );
  }


  getAutores(){
    this.autores_service.getAutores().subscribe(
      {
        next:(response) => {
          this.autores_data = response;
        },
        error:(err) =>{
        }
      }
    );
  }

 
  ngOnInit(): void {
    this.libros_data = '';
    this.categorias_data = '';
    this.autores_data = '';
   
  }

 
  crear() {
    this.mostrarModal = true;
    this.modalService.openModalCrear();
  }

  editar(l: LibroEntity) {
    this.mostrarModal = true;
    this.libro = l;
    this.modalService.openModalEditar();
  }

  eliminar(l: LibroEntity) {
    this.mostrarModal = true;
    this.libro = l;

    console.log(this.libro)
    this.modalService.openModalEliminar();
  }

 



}
