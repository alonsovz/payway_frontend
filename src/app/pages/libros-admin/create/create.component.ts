import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AutorEntity } from 'src/app/models/autor.entity';
import { CategoriaEntity } from 'src/app/models/categoria.entity';
import { LibroEntity } from 'src/app/models/libro.entity';
import { CategoriasService } from 'src/app/services/categorias.service';
import { LibrosService } from 'src/app/services/libros.service';
import { ModalService } from 'src/app/services/modal.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  form!: FormGroup;
  myGroup!: FormGroup;
  categorias_data: any;
  parametros: any;

  @Input()  categorias : any;
  @Input()  autores : any;

 

  constructor(public modalService: ModalService, private libros_service: LibrosService) {
    this.form = new FormGroup({
      'titulo' : new FormControl(''),
      'precio' : new FormControl(''),
      'autor_id' : new FormControl('---'),
      'categoria_id' : new FormControl('---'),
    });


  }

  ngOnInit(): void {
  
  }
  

  closeModal() {
    this.modalService.closeModalCrear();
  }

  save(){
   
   
    let categoria : CategoriaEntity = new CategoriaEntity();
    categoria.id = this.form.controls["categoria_id"].value;

    let autor: AutorEntity = new AutorEntity();
    autor.id = this.form.controls["autor_id"].value;

    let libro : LibroEntity = new LibroEntity();
    libro.autor = autor;
    libro.categoria = categoria;
    libro.titulo = this.form.controls["titulo"].value;
    libro.precio = this.form.controls["precio"].value;
    libro.estado = true;

    this.libros_service.save(libro).subscribe(
      {
        next: (response)=>{
          Swal.fire({
            icon: "success",
            title: "Confirmación!",
            text: "Libro guardado con éxito!",
          });
          this.libros_service.setRefresh(true);
          this.modalService.closeModalCrear();
        },error:(err)=>{
          Swal.fire({
            icon: "error",
            title: "Oops!",
            text: "Algo salió mal!",
          });
        }
      }
    );
  }


  
}
