import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AutorEntity } from 'src/app/models/autor.entity';
import { CategoriaEntity } from 'src/app/models/categoria.entity';
import { LibroEntity } from 'src/app/models/libro.entity';
import { LibrosService } from 'src/app/services/libros.service';
import { ModalService } from 'src/app/services/modal.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {
  form!: FormGroup;
  @Input()  libro : LibroEntity = new LibroEntity();

  constructor(public modalService: ModalService, private libros_service: LibrosService) { }

  ngOnInit(): void {
  }

  
  closeModal() {
    this.modalService.closeModalEliminar();
  }


  save(){
    let categoria : CategoriaEntity = new CategoriaEntity();
    categoria.id = this.libro.categoria.id;

    let autor: AutorEntity = new AutorEntity();
    autor.id = this.libro.autor.id;

    let libro : LibroEntity = new LibroEntity();
    libro.autor = autor;
    libro.categoria = categoria;
    libro.titulo = this.libro.titulo;
    libro.precio = this.libro.precio;
    libro.estado = false;
    libro.id = this.libro.id;

    this.libros_service.delete(libro).subscribe(
      {
        next: (response)=>{
          Swal.fire({
            icon: "success",
            title: "Confirmación!",
            text: "Libro eliminado con éxito!",
          });
          this.libros_service.setRefresh(true);
          this.modalService.closeModalEliminar();
        },error:(err)=>{
          Swal.fire({
            icon: "error",
            title: "Oops!",
            text: "Algo salió mal!",
          });
        }
      }
    );
  }


}
