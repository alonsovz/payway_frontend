import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewComponent } from './view/view.component';
const routes: Routes = [
  {
    path: '', component: ViewComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./../dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'main',
        loadChildren: () => import('./../main/main.module').then(m => m.MainModule)
      },
      {
        path: 'libros-admin',
        loadChildren: () => import('./../libros-admin/libros-admin.module').then(m => m.LibrosAdminModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
