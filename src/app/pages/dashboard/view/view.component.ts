import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  isActive = false;

  toggleBurger() {
    this.isActive = !this.isActive;
  }

  
  constructor() { }

  ngOnInit(): void {
  }

}
